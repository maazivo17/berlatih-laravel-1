<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register()
    {
        return view('register');
    }
    function welcome(Request $request)
    {
        $first = $request->firstname;
        $last = $request->lastname;
        return view('welcome', compact('first', 'last'));
    }
}
