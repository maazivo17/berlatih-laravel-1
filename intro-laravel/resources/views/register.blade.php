<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up!</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome">
        <label for="firstname">First Name: <br>
            <input type="text" id="firstname" name="firstname" required>
        </label><br><br>

        <label for="lastname">Last Name: <br>
            <input type="text" id="lastname" name="lastname" required>
        </label><br><br>

        Gender: <br>
        <label for="male">
            <input type="radio" id="male" name="gender" value="male" required>
            Male
        </label><br>
        <label for="female">
            <input type="radio" id="female" name="gender" value="female">
            Female
        </label><br>
        <label for="other">
            <input type="radio" id="other" name="gender" value="other">
            Other
        </label><br><br>

        Nationality: <br>
        <select name="nationality" id="nationality" required>
            <option value="idn">Indonesia</option>
            <option value="jpn">Japan</option>
            <option value="malay">Malaysia</option>
        </select><br><br>

        <label for="lang">Language Spoken:</label><br>
        <select name="lang" id="lang" required>
            <option value="idn">Bahasa Indonesia</option>
            <option value="eng">English</option>
            <option value="other">Other</option>
        </select><br><br>

        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <button id="signup">Sign Up!</button>
    </form>
</body>

</html>